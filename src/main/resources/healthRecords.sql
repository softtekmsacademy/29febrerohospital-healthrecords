CREATE DATABASE IF NOT EXISTS `healthrecords` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `healthrecords`;

-- Table structure for table `gynecology_obstetrical_history`
DROP TABLE IF EXISTS `gynecology_obstetrical_history`;
CREATE TABLE `gynecology_obstetrical_history` (
  `id_gynecology_obstetrical_history` int(11) NOT NULL AUTO_INCREMENT,
  `menarche` date DEFAULT NULL,
  `sexual_development` varchar(45) DEFAULT NULL,
  `menstrual_rhythm` varchar(45) DEFAULT NULL,
  `sex_life` varchar(45) DEFAULT NULL,
  `child_births` varchar(45) DEFAULT NULL,
  `abortions` int(2) DEFAULT NULL,
  `caesarean_sections` varchar(45) DEFAULT NULL,
  `contraceptive_meth` varchar(45) DEFAULT NULL,
  `menopause` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_gynecology_obstetrical_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table structure for table `non_pathological_personal_history`
DROP TABLE IF EXISTS `non_pathological_personal_history`;
CREATE TABLE `non_pathological_personal_history` (
  `id_non_pathological_personal_history` int(11) NOT NULL AUTO_INCREMENT,
  `bath` varchar(45) DEFAULT NULL,
  `defecation` varchar(45) DEFAULT NULL,
  `tooth_brushing` varchar(45) DEFAULT NULL,
  `room_conditions` varchar(45) DEFAULT NULL,
  `smoking` varchar(45) DEFAULT NULL,
  `alcoholism` varchar(45) DEFAULT NULL,
  `toxiconomies` varchar(45) DEFAULT NULL,
  `feeding` varchar(45) DEFAULT NULL,
  `sports` varchar(45) DEFAULT NULL,
  `scholarship` varchar(45) DEFAULT NULL,
  `immunizations` varchar(45) DEFAULT NULL,
  `allergies` varchar(45) DEFAULT NULL,
  `work_rest` varchar(45) DEFAULT NULL,
  `hobbies` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_non_pathological_personal_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table structure for table `pathological_personal_history`
DROP TABLE IF EXISTS `pathological_personal_history`;
CREATE TABLE `pathological_personal_history` (
  `id_pathological_personal_history` int(11) NOT NULL AUTO_INCREMENT,
  `child_infections` varchar(45) DEFAULT NULL,
  `venereal_dis` varchar(45) DEFAULT NULL,
  `surgical_interv` varchar(45) DEFAULT NULL,
  `traumas` varchar(45) DEFAULT NULL,
  `loss_of_consciousnes` varchar(45) DEFAULT NULL,
  `medicine_allergies` varchar(45) DEFAULT NULL,
  `blood_transfusions` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_pathological_personal_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table structure for table `health_records`
DROP TABLE IF EXISTS `health_records`;
CREATE TABLE `health_records` (
  `id_health_record` int(11) NOT NULL AUTO_INCREMENT,
  `record_entry_date` timestamp NULL DEFAULT NULL,
  `id_gynecology_obstetrical_history` int(11) DEFAULT NULL,
  `id_non_pathological_personal_history` int(11) NOT NULL,
  `id_pathological_personal_history` int(11) NOT NULL,
  PRIMARY KEY (`id_health_record`),
  KEY `fk_HealthRecord_gynecologyObstetricalHistory1_idx` (`id_gynecology_obstetrical_history`),
  KEY `fk_HealthRecord_nonpathologicalPersonalHistory1_idx` (`id_non_pathological_personal_history`),
  KEY `fk_HealthRecord_pathologicalPersonalHistory1_idx` (`id_pathological_personal_history`),
  CONSTRAINT `fk_HealthRecord_gynecologyObstetricalHistory1` FOREIGN KEY (`id_gynecology_obstetrical_history`) REFERENCES `gynecology_obstetrical_history` (`id_gynecology_obstetrical_history`),
  CONSTRAINT `fk_HealthRecord_nonpathologicalPersonalHistory1` FOREIGN KEY (`id_non_pathological_personal_history`) REFERENCES `non_pathological_personal_history` (`id_non_pathological_personal_history`),
  CONSTRAINT `fk_HealthRecord_pathologicalPersonalHistory1` FOREIGN KEY (`id_pathological_personal_history`) REFERENCES `pathological_personal_history` (`id_pathological_personal_history`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table structure for table `inherital_family_background`
DROP TABLE IF EXISTS `inherital_family_background`;
CREATE TABLE `inherital_family_background` (
  `id_inherital_family_background` int(11) NOT NULL AUTO_INCREMENT,
  `relationship` varchar(45) DEFAULT NULL,
  `alive` tinyint(4) DEFAULT NULL,
  `age` int(10) unsigned DEFAULT NULL,
  `ailing` varchar(45) DEFAULT NULL,
  `current_health_state` varchar(45) DEFAULT NULL,
  `id_health_record` int(11) NOT NULL,
  PRIMARY KEY (`id_inherital_family_background`),
  KEY `fk_inheritFamily_HealthRecord1_idx` (`id_health_record`),
  CONSTRAINT `fk_inheritFamily_HealthRecord1` FOREIGN KEY (`id_health_record`) REFERENCES `health_records` (`id_health_record`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
