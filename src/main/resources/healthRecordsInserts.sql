USE `healthrecords`;

--	Gynecology Obstetrical History Inserts

INSERT INTO `gynecology_obstetrical_history` (`id_gynecology_obstetrical_history`, `menarche`, `sexual_development`, `menstrual_rhythm`, `sex_life`, `child_births`, `abortions`, `caesarean_sections`, `contraceptive_meth`, `menopause`) VALUES
	(1, '2000-12-04', 'pubic hair growing', 'each 28 day', 'Started her sexual relations in younger age', 'none', 0, 'none', 'Condom', 'none'),
	(2, '1959-10-10', 'mature', 'each month', 'Started her sexual relations in a middle age', '2', 0, '1', 'none', 'none'),
	(3, '1980-08-24', 'mature', 'each month', 'Started her sexual relations in a middle age', '5', 0, '1', 'none', 'at age of 48'),
	(4, '0000-00-00', 'young', 'none', 'not sexual relations yet', '0', 0, '0', 'none', 'none'),
	(5, '2000-12-04', 'pubic hair growing', 'each 28 day', 'Started her sexual relations in younger age', 'none', 0, 'none', 'none', 'none'),
	(6, '0000-00-00', 'young', 'none', 'not sexual relations yet', '0', 0, '0', 'none', 'none'),
	(7, '1959-10-10', 'mature', 'each month', 'Started her sexual relations in a middle age', '2', 0, '1', 'pill', 'none'),
	(8, '0000-00-00', 'young', 'none', 'not sexual relations yet', '0', 0, '0', 'none', 'none'),
	(9, '2000-12-04', 'pubic hair growing', 'each 28 day', 'Started her sexual relations in younger age', 'none', 0, 'none', 'none', 'none');
		
--	Non Pathological Personal History Inserts

INSERT INTO `non_pathological_personal_history` (`id_non_pathological_personal_history`, `bath`, `defecation`, `tooth_brushing`, `room_conditions`, `smoking`, `alcoholism`, `toxiconomies`, `feeding`, `sports`, `scholarship`, `immunizations`, `allergies`, `work_rest`, `hobbies`) VALUES
	(1, 'Todos los dias', 'Normal', '3', 'Clean', 'no', 'no', 'no', 'Good', 'no', 'PHD', 'No', 'None', 'Sunday', 'dancing'),
	(2, 'Cada 2 dias', 'Verde', '2', 'Dusty', 'yes', 'no', 'no', 'Good', 'yes', 'High School', 'No', 'None', 'Saturday', 'Singing'),
	(3, 'Todos los dias', 'Buena', '2', 'Dusty', 'no', 'yes', 'yes', 'good', 'yes', 'PHD', 'No', 'None', 'Saturday', 'Gambling'),
	(4, 'Cada 2 o 3 dias', 'Normal', '3', 'Clean', 'yes', 'yes', 'yes', 'bad', 'no', 'High School', 'Yes', 'None', 'Sunday', 'Drink and party hard'),
	(5, 'Diario', 'Normal', '3', 'No lights', 'yes', 'yes', 'yes', 'good', 'no', 'None', 'No', 'none', 'night shifts', 'Hiking'),
	(6, 'Más o menos', 'Normal', '2', 'Humidity', 'no', 'no', 'no', 'good', 'no', 'Masters', 'No', 'ampicilin', 'sundays only', 'Movies'),
	(7, 'Todos los dias', 'Verde', '3', 'Dusty', 'no', 'no', 'no', 'bad', 'yes', 'PHD', 'No', 'cats', 'weekends onlu', 'Traveling'),
	(8, 'Cada 2 dias', 'Buena', '3', 'Clean', 'yes', 'yes', 'yes', 'bad', 'yes', 'High School', 'No', 'love', 'weekends onlu', 'Activisim'),
	(9, 'Todos los dias', 'Normal', '2', 'Clean', 'no', 'yes', 'no', 'bad', 'no', 'None', 'No', 'none', 'weekends onlu', 'Con artist');
	
-- Pathological Personal History Inserts

INSERT INTO `pathological_personal_history` (`id_pathological_personal_history`, `child_infections`, `venereal_dis`, `surgical_interv`, `traumas`, `loss_of_consciousnes`, `medicine_allergies`, `blood_transfusions`) VALUES
	(1, 'None', 'None', 'None', 'None', '5', 'None', 'None'),
	(2, 'None', 'None', '2', 'None', 'None', 'None', 'None'),
	(3, 'None', 'None', 'None', '1', '1', 'None', '2'),
	(4, 'Bees', 'None', 'None', 'None', 'None', 'Ampicilination', 'None'),
	(5, 'None', 'VIH', '2', 'None', 'None', 'None', 'None'),
	(6, 'Cats', 'None', 'None', '1', 'None', 'None', '1'),
	(7, 'None', 'None', '1', 'None', 'None', 'Ambroxol', 'None'),
	(8, 'None', 'None', 'None', 'None', 'None', 'None', 'None'),
	(9, 'Bullets', 'None', '4', '1', '2', 'Aspirin', '2'),
	(10, 'None', 'None', 'None', 'None', 'None', 'None', 'None'),
	(11, 'Dogs', 'None', 'None', 'None', '4', 'None', '3'),
	(12, 'None', 'None', 'None', 'None', 'None', 'None', 'None');
	
--	Health Records Inserts
	
	INSERT INTO `health_records` (`id_health_record`, `record_entry_date`, `id_gynecology_obstetrical_history`, `id_non_pathological_personal_history`, `id_pathological_personal_history`) VALUES
	(1, '2020-01-08 09:54:43', NULL, 1, 1),
	(2, '2020-01-08 09:54:44', NULL, 2, 2),
	(3, '2020-01-08 09:54:45', NULL, 3, 3),
	(4, '2020-01-08 09:54:46', NULL, 4, 4),
	(5, '2020-01-08 09:54:46', NULL, 5, 5),
	(6, '2020-01-08 09:54:47', 1, 6, 6),
	(7, '2020-01-08 09:54:47', 2, 7, 7),
	(8, '2020-01-08 09:54:48', 3, 8, 8),
	(9, '2020-01-08 09:54:49', 4, 9, 9);
	
--	Inherital Family Background Inserts

INSERT INTO `inherital_family_background` (`id_inherital_family_background`, `relationship`, `alive`, `age`, `ailing`, `current_health_state`, `id_health_record`) VALUES
	(1, 'Mother', 1, 50, 'none', 'fine', 1),
	(2, 'Father', 1, 49, 'none', 'fine', 1),
	(3, 'Sister', 1, 12, 'none', 'fine', 1),
	(4, 'Brother', 1, 15, 'none', 'fine', 1),
	(5, 'Cousin', 1, 29, 'Diabetes', 'fine', 1);
