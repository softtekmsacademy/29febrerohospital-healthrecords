package com.softtek.project.hospital.healthrecords.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.project.hospital.healthrecords.model.InheritalFamilyBackground;
import com.softtek.project.hospital.healthrecords.repository.InheritalFamilyBackgroundRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )

public class InheritalFamilyBackgroundServiceImpl implements InheritalFamilyBackgroundService {
    @Autowired
    InheritalFamilyBackgroundRepository inheritalFamilyBackgroundRepository;

    @HystrixCommand(threadPoolKey = "getAllThreadPool", fallbackMethod = "buildFallback")
    @Override
    public List<InheritalFamilyBackground> findAllInheritalFamilyBackgroundByHealthRecordId(Integer Id) {

        List<InheritalFamilyBackground> unfiltered = inheritalFamilyBackgroundRepository.findAll();

        Stream<InheritalFamilyBackground> filtered = unfiltered.stream()
                .filter(inherital ->
                        inherital.getHealthRecord().getId_health_record().equals(Id));

        return filtered.collect(Collectors.toList());
    }

    @HystrixCommand(threadPoolKey = "createThreadPool")
    @Override
    public InheritalFamilyBackground createInheritalFamilyBackground(InheritalFamilyBackground inheritalFamilyBackground) {
        return inheritalFamilyBackgroundRepository.save(inheritalFamilyBackground);
    }

    @HystrixCommand(threadPoolKey = "updateThreadPool")
    @Override
    public InheritalFamilyBackground updateInheritalFamilyBackgroundById(InheritalFamilyBackground inheritalFamilyBackground) {
        return inheritalFamilyBackgroundRepository.save(inheritalFamilyBackground);
    }

    @HystrixCommand(threadPoolKey = "getThreadPool", fallbackMethod = "buildFallback")
    @Override
    public InheritalFamilyBackground findInheritalFamilyBackgroundById(Integer inheritalFamilyBackgroundId) {
        return inheritalFamilyBackgroundRepository.findById(inheritalFamilyBackgroundId).orElse(null);
    }

    @HystrixCommand
    @Override
    public InheritalFamilyBackground buildFallback(Integer id) {
        InheritalFamilyBackground fallback = new InheritalFamilyBackground();
        fallback.setRelationship("No info found");
        fallback.setCurrentHealthState("No info found");
        fallback.setAlive(0);
        fallback.setAiling("No info found");
        fallback.setAge(0);
        fallback.setHealthRecord(null);
        return fallback;
    }
}
