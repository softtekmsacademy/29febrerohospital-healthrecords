package com.softtek.project.hospital.healthrecords.repository;

import com.softtek.project.hospital.healthrecords.model.HealthRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthRecordRepository extends JpaRepository<HealthRecord, Integer> {

}

