package com.softtek.project.hospital.healthrecords.service;

import com.softtek.project.hospital.healthrecords.model.NonPathologicalPersonalHistory;

public interface NonPathologicalPersonalHistoryService {
    NonPathologicalPersonalHistory findNonPathologicalPersonalHistoryById(Integer healthRecordId);
    NonPathologicalPersonalHistory updateNonPathologicalPersonalHistory(NonPathologicalPersonalHistory newNonPathologicalPersonalHistory);
    NonPathologicalPersonalHistory buildFallback(Integer id);
}
