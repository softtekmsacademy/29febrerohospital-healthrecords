package com.softtek.project.hospital.healthrecords.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.project.hospital.healthrecords.model.GynecologyObstetricalHistory;
import com.softtek.project.hospital.healthrecords.model.HealthRecord;
import com.softtek.project.hospital.healthrecords.model.NonPathologicalPersonalHistory;
import com.softtek.project.hospital.healthrecords.model.PathologicalPersonalHistory;
import com.softtek.project.hospital.healthrecords.repository.HealthRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )
public class HealthRecordServiceImpl implements HealthRecordService {

    @Autowired
    private HealthRecordRepository healthRecordRepository;

    @HystrixCommand(threadPoolKey = "getThreadPool", fallbackMethod = "buildFallback")
    public HealthRecord findHealthRecordById(Integer healthRecordId){
        return healthRecordRepository.findById(healthRecordId).orElse(null);
    }

    @HystrixCommand
    @Override
    public HealthRecord buildFallback(Integer id) {
        HealthRecord fallback = new HealthRecord();
        fallback.setGynecologyObstetricalHistory(new GynecologyObstetricalHistory());
        fallback.setNonpathologicalPersonalHistory(new NonPathologicalPersonalHistory());
        fallback.setPathologicalPersonalHistory(new PathologicalPersonalHistory());
        fallback.setRecordEntryDate(new Date());
        return fallback;
    }


}
