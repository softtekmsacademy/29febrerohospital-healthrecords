package com.softtek.project.hospital.healthrecords.model;
import javax.persistence.*;

@Entity(name = "PathologicalPersonalHistory")
@Table(name = "pathological_personal_history")
public class PathologicalPersonalHistory {

    @Id
    @Column(name = "id_pathological_personal_history")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdPathologicalPersonalHistory;
    private String childInfections;
    private String venerealDis;
    private String surgicalInterv;
    private String traumas;
    private String lossOfConsciousnes;
    private String medicineAllergies;
    private String bloodTransfusions;

    public PathologicalPersonalHistory(){}

    public PathologicalPersonalHistory(String childInfections, String venerealDis, String surgicalInterv, String traumas, String lossOfConsciousnes, String medicineAllergies, String bloodTransfusions) {
        this.childInfections = childInfections;
        this.venerealDis = venerealDis;
        this.surgicalInterv = surgicalInterv;
        this.traumas = traumas;
        this.lossOfConsciousnes = lossOfConsciousnes;
        this.medicineAllergies = medicineAllergies;
        this.bloodTransfusions = bloodTransfusions;
    }

    public String getChildInfections() {
        return childInfections;
    }

    public void setChildInfections(String childInfections) {
        this.childInfections = childInfections;
    }

    public String getVenerealDis() {
        return venerealDis;
    }

    public void setVenerealDis(String venerealDis) {
        this.venerealDis = venerealDis;
    }

    public String getSurgicalInterv() {
        return surgicalInterv;
    }

    public void setSurgicalInterv(String surgicalInterv) {
        this.surgicalInterv = surgicalInterv;
    }

    public String getTraumas() {
        return traumas;
    }

    public void setTraumas(String traumas) {
        this.traumas = traumas;
    }

    public String getLossOfConsciousnes() {
        return lossOfConsciousnes;
    }

    public void setLossOfConsciousnes(String lossOfConsciousnes) {
        this.lossOfConsciousnes = lossOfConsciousnes;
    }

    public String getMedicineAllergies() {
        return medicineAllergies;
    }

    public void setMedicineAllergies(String medicineAllergies) {
        this.medicineAllergies = medicineAllergies;
    }

    public String getBloodTransfusions() {
        return bloodTransfusions;
    }

    public void setBloodTransfusions(String bloodTransfusions) {
        this.bloodTransfusions = bloodTransfusions;
    }

    public Integer getId_pathological_personal_history() {
        return IdPathologicalPersonalHistory;
    }

    public void setId_pathological_personal_history(Integer IdPathologicalPersonalHistory) {
        this.IdPathologicalPersonalHistory = IdPathologicalPersonalHistory;
    }

}