package com.softtek.project.hospital.healthrecords.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.project.hospital.healthrecords.model.GynecologyObstetricalHistory;
import com.softtek.project.hospital.healthrecords.repository.GynecologyObstetricalHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )

public class GynecologyObstetricalHistoryServiceImpl implements GynecologyObstetricalHistoryService {
    @Autowired
    GynecologyObstetricalHistoryRepository gynecologyObstetricalHistoryRepository;

    @HystrixCommand(threadPoolKey = "getThreadPool", fallbackMethod = "buildFallback")
    @Override
    public GynecologyObstetricalHistory findGynecologyObstetricalHistoryById(Integer id) {
        return gynecologyObstetricalHistoryRepository.findById(id).orElse(null);
    }

    @HystrixCommand(threadPoolKey = "updateThreadPool")
    @Override
    public GynecologyObstetricalHistory updateGynecologyObstetricalHistory(GynecologyObstetricalHistory newGynecologyObstetricalHistory) {
        Integer id = newGynecologyObstetricalHistory.getId();

        GynecologyObstetricalHistory oldGynecologyObstetricalHistory = gynecologyObstetricalHistoryRepository.findById(id).get();

        oldGynecologyObstetricalHistory.setAbortions(newGynecologyObstetricalHistory.getAbortions());
        oldGynecologyObstetricalHistory.setCesareanSections(newGynecologyObstetricalHistory.getCesareanSections());
        oldGynecologyObstetricalHistory.setChildBirths(newGynecologyObstetricalHistory.getChildBirths());
        oldGynecologyObstetricalHistory.setMenarche(newGynecologyObstetricalHistory.getMenarche());
        oldGynecologyObstetricalHistory.setMenopause(newGynecologyObstetricalHistory.getMenopause());
        oldGynecologyObstetricalHistory.setMenstrualRhythm(newGynecologyObstetricalHistory.getMenstrualRhythm());
        oldGynecologyObstetricalHistory.setContraceptiveMeth(newGynecologyObstetricalHistory.getContraceptiveMeth());
        oldGynecologyObstetricalHistory.setSexLife(newGynecologyObstetricalHistory.getSexLife());
        oldGynecologyObstetricalHistory.setSexualDevelopment(newGynecologyObstetricalHistory.getSexualDevelopment());
        return gynecologyObstetricalHistoryRepository.save(oldGynecologyObstetricalHistory);
    }

    @HystrixCommand
    @Override
    public GynecologyObstetricalHistory buildFallback(Integer id) {
        GynecologyObstetricalHistory fallback = new GynecologyObstetricalHistory();
        fallback.setAbortions(0);
        fallback.setCesareanSections("no info found");
        fallback.setChildBirths("no info found");
        fallback.setMenarche("no info found");
        fallback.setMenopause("no info found");
        fallback.setMenstrualRhythm("no info found");
        fallback.setContraceptiveMeth("no info found");
        fallback.setSexLife("no info found");
        fallback.setSexualDevelopment("no info found");
        return fallback;
    }

}
