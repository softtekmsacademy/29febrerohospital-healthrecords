package com.softtek.project.hospital.healthrecords.controller;

import com.softtek.project.hospital.healthrecords.model.HealthRecord;
import com.softtek.project.hospital.healthrecords.model.InheritalFamilyBackground;
import com.softtek.project.hospital.healthrecords.service.HealthRecordService;
import com.softtek.project.hospital.healthrecords.service.InheritalFamilyBackgroundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller // This means that this class is a Controller
@RequestMapping(path="/healthRecord") // This means URL's start with /healthRecord (after Application path)
public class InheritalFamilyBackgroundController {
    @Autowired // This means to get the bean called health Record
    private HealthRecordService healthRecordService;

    @Autowired // This means to get the bean called inherital family background
    private InheritalFamilyBackgroundService inheritalFamilyBackgroundService;

    @Autowired
    public InheritalFamilyBackgroundController(){
    }

    @GetMapping(path="/{healthRecordId}/inheritalFamilyBackground")
    public @ResponseBody
    ResponseEntity<String> getInheritalFamilyBackgroundByHealthRecordId(
            @PathVariable(value="healthRecordId") Integer healthRecordId) {
        HealthRecord hr = healthRecordService.findHealthRecordById(healthRecordId);

        if(hr != null){
            try{

                List<InheritalFamilyBackground> list = inheritalFamilyBackgroundService.findAllInheritalFamilyBackgroundByHealthRecordId(healthRecordId);

                if(list.size()>0){
                    return new ResponseEntity(list, HttpStatus.OK);
                }else{
                    return new ResponseEntity<>("No entries were found for the health record provided", HttpStatus.CONFLICT);
                }
            }catch (Exception e){
                return new ResponseEntity<>("Error getting inherital family background",HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }
    }

    @PostMapping(path="/{healthRecordId}/inheritalFamilyBackground", consumes = "application/json")
    public @ResponseBody
    ResponseEntity<String> postInheritalFamilyBackgroundByHealthRecordId(@PathVariable(value="healthRecordId") Integer healthRecordId, @RequestBody InheritalFamilyBackground body) {

        HealthRecord hr = healthRecordService.findHealthRecordById(healthRecordId);
        if(hr != null){
            try{
                body.setHealthRecord(hr);
                inheritalFamilyBackgroundService.createInheritalFamilyBackground(body);
                return new ResponseEntity<>("inherital family background saved", HttpStatus.CONFLICT);
            }catch (Exception e){
                return new ResponseEntity<>("Inherital family background not saved",HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }
    }

    @PutMapping(path="/{healthRecordId}/inheritalFamilyBackground/{idInheritalFamilyBackground}", consumes = "application/json")
    public @ResponseBody
    ResponseEntity<String> putInheritalFamilyBackgroundByHealthRecordId(@PathVariable(value="healthRecordId") Integer healthRecordId,
                                                                        @PathVariable(value="idInheritalFamilyBackground") Integer idInheritalFamilyBackground,
                                                                        @RequestBody InheritalFamilyBackground body) {

        HealthRecord hr = healthRecordService.findHealthRecordById(healthRecordId);

        if(hr != null){
            try{
                body.setHealthRecord(hr);
                InheritalFamilyBackground inheritalFamilyBackground = inheritalFamilyBackgroundService.findInheritalFamilyBackgroundById(idInheritalFamilyBackground);

                if(inheritalFamilyBackground != null){
                    if(inheritalFamilyBackground.getHealthRecord().getId_health_record().equals(healthRecordId)) {
                        body.setId_inherital_family_background(idInheritalFamilyBackground);
                        InheritalFamilyBackground save = inheritalFamilyBackgroundService.updateInheritalFamilyBackgroundById(body);
                        if(save != null){
                            return new ResponseEntity<>("inherital family background saved", HttpStatus.OK);
                        }else{
                            return new ResponseEntity<>("Error while saving", HttpStatus.CONFLICT);
                        }
                    }else{
                        return new ResponseEntity<>("Health record id does not correspond to the inherital family background", HttpStatus.CONFLICT);
                    }
                }else
                    return new ResponseEntity<>("inherital family background does not exist", HttpStatus.CONFLICT);
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }
    }

}