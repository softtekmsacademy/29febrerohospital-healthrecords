package com.softtek.project.hospital.healthrecords.service;

import com.softtek.project.hospital.healthrecords.model.GynecologyObstetricalHistory;

public interface GynecologyObstetricalHistoryService {
    GynecologyObstetricalHistory findGynecologyObstetricalHistoryById(Integer healthRecordId);
    GynecologyObstetricalHistory updateGynecologyObstetricalHistory(GynecologyObstetricalHistory newGynecologyObstetricalHistory);
    GynecologyObstetricalHistory buildFallback(Integer id);
}
