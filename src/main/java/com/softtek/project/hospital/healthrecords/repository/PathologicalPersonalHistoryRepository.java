package com.softtek.project.hospital.healthrecords.repository;

import com.softtek.project.hospital.healthrecords.model.PathologicalPersonalHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PathologicalPersonalHistoryRepository extends JpaRepository<PathologicalPersonalHistory, Integer> {

}

