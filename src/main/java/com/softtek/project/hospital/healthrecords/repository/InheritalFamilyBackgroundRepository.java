package com.softtek.project.hospital.healthrecords.repository;

import com.softtek.project.hospital.healthrecords.model.InheritalFamilyBackground;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InheritalFamilyBackgroundRepository extends JpaRepository<InheritalFamilyBackground, Integer> {

}

