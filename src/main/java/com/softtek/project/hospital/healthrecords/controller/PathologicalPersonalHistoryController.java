package com.softtek.project.hospital.healthrecords.controller;

import com.softtek.project.hospital.healthrecords.model.PathologicalPersonalHistory;
import com.softtek.project.hospital.healthrecords.model.HealthRecord;
import com.softtek.project.hospital.healthrecords.service.PathologicalPersonalHistoryService;
import com.softtek.project.hospital.healthrecords.service.HealthRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/healthRecord") // This means URL's start with /healthRecord (after Application path)
public class PathologicalPersonalHistoryController {
    @Autowired
    private PathologicalPersonalHistoryService PathologicalPersonalHistoryService;

    @Autowired
    private HealthRecordService healthRecordService;

    @Autowired
    public PathologicalPersonalHistoryController(){
    }

    @GetMapping(path="/{healthRecordId}/pathologicalpersonal",
            produces = "application/json")
    public @ResponseBody
    ResponseEntity<String> getPathologicalPersonalHistoryByHealthRecordId(
            @PathVariable(value="healthRecordId") Integer healthRecordId) {

        PathologicalPersonalHistory pathologicalPersonalHistory = null;
        HealthRecord hr = healthRecordService.findHealthRecordById(healthRecordId);

        if(hr != null) {
            pathologicalPersonalHistory = hr.getPathologicalPersonalHistory();
            if(pathologicalPersonalHistory != null) {
                Integer id = pathologicalPersonalHistory.getId_pathological_personal_history();

                pathologicalPersonalHistory = PathologicalPersonalHistoryService.findPathologicalPersonalHistoryById(id);
                return new ResponseEntity(pathologicalPersonalHistory, HttpStatus.OK);
            }else{
                return new ResponseEntity<>("health record does not have  pathological personal history", HttpStatus.CONFLICT);
            }
        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }
    }

    @PutMapping(path="/{healthRecordId}/pathologicalpersonal",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody
    ResponseEntity<String> putPathologicalPersonalHistoryById(
            @PathVariable(value="healthRecordId") Integer healthRecordId,
            @RequestBody PathologicalPersonalHistory newPathologicalPersonalHistory) {

        Integer idPath;
        PathologicalPersonalHistory oldPathologicalPersonalHistory;

        HealthRecord hr = healthRecordService.findHealthRecordById(healthRecordId);

        if(hr != null) {
            oldPathologicalPersonalHistory = hr.getPathologicalPersonalHistory();
            if (oldPathologicalPersonalHistory != null) {
                idPath = oldPathologicalPersonalHistory.getId_pathological_personal_history();
                newPathologicalPersonalHistory.setId_pathological_personal_history(idPath);

                PathologicalPersonalHistory update = PathologicalPersonalHistoryService.updatePathologicalPersonalHistory(newPathologicalPersonalHistory);

                if(update != null){
                    return new ResponseEntity<>(" pathological personal history updated", HttpStatus.OK);
                }else {
                    return new ResponseEntity<>(" pathological personal history not updated",HttpStatus.INTERNAL_SERVER_ERROR);
                }

            }else{
                return new ResponseEntity<>("health record does not have  pathological personal history", HttpStatus.CONFLICT);
            }
        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }


    }


}
