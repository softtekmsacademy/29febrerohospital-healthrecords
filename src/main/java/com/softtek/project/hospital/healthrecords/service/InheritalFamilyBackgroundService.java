package com.softtek.project.hospital.healthrecords.service;

import com.softtek.project.hospital.healthrecords.model.InheritalFamilyBackground;

import java.util.List;

public interface InheritalFamilyBackgroundService {
    List<InheritalFamilyBackground> findAllInheritalFamilyBackgroundByHealthRecordId(Integer healthRecordId);

    InheritalFamilyBackground createInheritalFamilyBackground(InheritalFamilyBackground inheritalFamilyBackground);

    InheritalFamilyBackground updateInheritalFamilyBackgroundById(InheritalFamilyBackground inheritalFamilyBackground);

    InheritalFamilyBackground findInheritalFamilyBackgroundById(Integer inheritalFamilyBackgroundId);

    InheritalFamilyBackground buildFallback(Integer id);
}
