package com.softtek.project.hospital.healthrecords.controller;

import com.softtek.project.hospital.healthrecords.model.GynecologyObstetricalHistory;
import com.softtek.project.hospital.healthrecords.model.HealthRecord;
import com.softtek.project.hospital.healthrecords.service.GynecologyObstetricalHistoryService;
import com.softtek.project.hospital.healthrecords.service.HealthRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/healthRecord") // This means URL's start with /healthRecord (after Application path)
public class GynecologyObstetricalController {
    @Autowired
    private GynecologyObstetricalHistoryService gynecologyObstetricalHistoryService;

    @Autowired
    private HealthRecordService healthRecordService;

    @Autowired
    public GynecologyObstetricalController(){
    }

    @GetMapping(path="/{healthRecordId}/gynecologyobstetrical",
            produces = "application/json")
    public @ResponseBody
    ResponseEntity<String> getGynecologyObstetricalHistoryByHealthRecordId(
            @PathVariable(value="healthRecordId") Integer healthRecordId) {

        GynecologyObstetricalHistory gynecologyObstetricalHistory = null;
        HealthRecord hr = healthRecordService.findHealthRecordById(healthRecordId);

        if(hr != null) {
            gynecologyObstetricalHistory = hr.getGynecologyObstetricalHistory();
            if(gynecologyObstetricalHistory != null) {
                Integer id = gynecologyObstetricalHistory.getId();

                gynecologyObstetricalHistory = gynecologyObstetricalHistoryService.findGynecologyObstetricalHistoryById(id);

                try {
                    return new ResponseEntity(gynecologyObstetricalHistory, HttpStatus.OK);
                }catch (Exception e){
                    return new ResponseEntity<>("Error getting Gynecology Obstetrical History", HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }else{
                return new ResponseEntity<>("health record does not have gynecology", HttpStatus.CONFLICT);
            }
        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }
    }

    @GetMapping(path="/gynecologyobstetrical/{gynecologyobstetricalId}",
            produces = "application/json")
    public @ResponseBody
    ResponseEntity<String> getGynecologyObstetricalHistoryById(
            @PathVariable(value="gynecologyobstetricalId")
                    Integer gynecologyobstetricalId) {

        GynecologyObstetricalHistory gynecologyObstetricalHistory;

        gynecologyObstetricalHistory =
                gynecologyObstetricalHistoryService.findGynecologyObstetricalHistoryById(gynecologyobstetricalId);

        if(gynecologyObstetricalHistory != null){
            try{
                return new ResponseEntity(gynecologyObstetricalHistory, HttpStatus.OK);
            }catch (Exception e){
                return new ResponseEntity<>("Error getting Gynecology Obstetrical History", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }else{
            return new ResponseEntity<>("gynecology history id not found",
                    HttpStatus.CONFLICT);
        }
    }

    @PutMapping(path="/{healthRecordId}/gynecologyobstetrical",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody
    ResponseEntity<String> putGynecologyObstetricalHistoryById(
            @PathVariable(value="healthRecordId") Integer healthRecordId,
            @RequestBody GynecologyObstetricalHistory newGynecologyObstetricalHistory) {

        Integer idGyn;
        GynecologyObstetricalHistory oldGynecologyObstetricalHistory;

        HealthRecord hr = healthRecordService.findHealthRecordById(healthRecordId);

        if(hr != null) {
            oldGynecologyObstetricalHistory = hr.getGynecologyObstetricalHistory();
            if (oldGynecologyObstetricalHistory != null) {
                idGyn = oldGynecologyObstetricalHistory.getId();
                newGynecologyObstetricalHistory.setId(idGyn);

                GynecologyObstetricalHistory update = gynecologyObstetricalHistoryService.updateGynecologyObstetricalHistory(newGynecologyObstetricalHistory);

                if(update != null){
                    return new ResponseEntity<>("gynecology history id updated", HttpStatus.OK);
                }else {
                    return new ResponseEntity<>("Gynecology history not updated",HttpStatus.INTERNAL_SERVER_ERROR);
                }

            }else{
                return new ResponseEntity<>("health record does not have gynecology history", HttpStatus.CONFLICT);
            }
        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }


    }


}
