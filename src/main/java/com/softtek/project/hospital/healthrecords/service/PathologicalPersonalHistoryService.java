package com.softtek.project.hospital.healthrecords.service;

import com.softtek.project.hospital.healthrecords.model.PathologicalPersonalHistory;

public interface PathologicalPersonalHistoryService {
    PathologicalPersonalHistory findPathologicalPersonalHistoryById(Integer healthRecordId);
    PathologicalPersonalHistory updatePathologicalPersonalHistory(PathologicalPersonalHistory newPathologicalPersonalHistory);
    PathologicalPersonalHistory buildFallback(Integer id);
}
