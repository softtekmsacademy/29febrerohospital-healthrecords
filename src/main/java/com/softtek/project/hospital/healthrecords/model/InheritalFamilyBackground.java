package com.softtek.project.hospital.healthrecords.model;
import javax.persistence.*;


@Entity
@Table(name = "inherital_family_background")
public class InheritalFamilyBackground {

    @Id
    @Column(name = "id_inherital_family_background")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdInheritalFamilyBackground;
    private String relationship;
    private Integer alive;
    private Integer age;
    private String ailing;
    private String currentHealthState;

    @OneToOne
    @JoinColumn(name = "id_health_record", referencedColumnName = "id_health_record")
    private HealthRecord healthRecord;

    public Integer getId_inherital_family_background() {
        return IdInheritalFamilyBackground;
    }

    public void setId_inherital_family_background(Integer IdInheritalFamilyBackground) {
        this.IdInheritalFamilyBackground = IdInheritalFamilyBackground;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public Integer getAlive() {
        return alive;
    }

    public void setAlive(Integer alive) {
        this.alive = alive;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAiling() {
        return ailing;
    }

    public void setAiling(String ailing) {
        this.ailing = ailing;
    }

    public String getCurrentHealthState() {
        return currentHealthState;
    }

    public void setCurrentHealthState(String currentHealthState) {
        this.currentHealthState = currentHealthState;
    }

    public HealthRecord getHealthRecord() {
        return healthRecord;
    }

    public void setHealthRecord(HealthRecord healthRecord) {
        this.healthRecord = healthRecord;
    }

    public InheritalFamilyBackground() {
    }

    public InheritalFamilyBackground(Integer IdInheritalFamilyBackground, String relationship, Integer alive, Integer age, String ailing, String currentHealthState, HealthRecord healthRecord) {
        this.IdInheritalFamilyBackground = IdInheritalFamilyBackground;
        this.relationship = relationship;
        this.alive = alive;
        this.age = age;
        this.ailing = ailing;
        this.currentHealthState = currentHealthState;
        this.healthRecord = healthRecord;
    }

}
