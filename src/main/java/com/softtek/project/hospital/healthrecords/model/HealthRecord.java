package com.softtek.project.hospital.healthrecords.model;

import java.util.Date;
import javax.persistence.*;


@Entity
@Table(name = "health_records")
public class HealthRecord {

    @Id
    @Column(name = "id_health_record")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdHealthRecord;

    private Date recordEntryDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pathological_personal_history", referencedColumnName = "id_pathological_personal_history")
    private PathologicalPersonalHistory pathologicalPersonalHistory;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_non_pathological_personal_history", referencedColumnName = "id_non_pathological_personal_history")
    private NonPathologicalPersonalHistory nonpathologicalPersonalHistory;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_gynecology_obstetrical_history", referencedColumnName = "id_gynecology_obstetrical_history")

    private GynecologyObstetricalHistory gynecologyObstetricalHistory;


    public HealthRecord(){}

    public HealthRecord(Integer IdHealthRecord, Date recordEntryDate) {
        this.IdHealthRecord = IdHealthRecord;
        this.recordEntryDate = recordEntryDate;
    }
    public HealthRecord(PathologicalPersonalHistory pathologicalPersonalHistory, NonPathologicalPersonalHistory nonpathologicalPersonalHistory,GynecologyObstetricalHistory gynecologyObstetricalHistory) {
        this.pathologicalPersonalHistory = pathologicalPersonalHistory;
        this.nonpathologicalPersonalHistory = nonpathologicalPersonalHistory;
        this.gynecologyObstetricalHistory = gynecologyObstetricalHistory;

    }

    public Integer getId_health_record() {
        return IdHealthRecord;
    }

    public void setId_health_record(Integer IdHealthRecord) {
        this.IdHealthRecord = IdHealthRecord;
    }

    public Date getRecordEntryDate() {
        return recordEntryDate;
    }

    public void setRecordEntryDate(Date recordEntryDate) {
        this.recordEntryDate = recordEntryDate;
    }

    public PathologicalPersonalHistory getPathologicalPersonalHistory() {
        return pathologicalPersonalHistory;
    }

    public void setPathologicalPersonalHistory(PathologicalPersonalHistory pathologicalPersonalHistory) {
        this.pathologicalPersonalHistory = pathologicalPersonalHistory;
    }

    public NonPathologicalPersonalHistory getNonpathologicalPersonalHistory() {
        return nonpathologicalPersonalHistory;
    }

    public void setNonpathologicalPersonalHistory(NonPathologicalPersonalHistory nonpathologicalPersonalHistory) {
        this.nonpathologicalPersonalHistory = nonpathologicalPersonalHistory;
    }

    public GynecologyObstetricalHistory getGynecologyObstetricalHistory() {
        return gynecologyObstetricalHistory;
    }

    public void setGynecologyObstetricalHistory(GynecologyObstetricalHistory gynecologyObstetricalHistory) {
        this.gynecologyObstetricalHistory = gynecologyObstetricalHistory;
    }

}
