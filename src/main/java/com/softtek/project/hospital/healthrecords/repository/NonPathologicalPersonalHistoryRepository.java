package com.softtek.project.hospital.healthrecords.repository;

import com.softtek.project.hospital.healthrecords.model.NonPathologicalPersonalHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NonPathologicalPersonalHistoryRepository extends JpaRepository<NonPathologicalPersonalHistory, Integer> {

}

