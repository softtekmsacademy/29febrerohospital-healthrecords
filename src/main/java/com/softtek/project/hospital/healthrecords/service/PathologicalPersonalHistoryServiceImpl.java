package com.softtek.project.hospital.healthrecords.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.project.hospital.healthrecords.model.PathologicalPersonalHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )

public class PathologicalPersonalHistoryServiceImpl implements PathologicalPersonalHistoryService {
    @Autowired
    com.softtek.project.hospital.healthrecords.repository.PathologicalPersonalHistoryRepository PathologicalPersonalHistoryRepository;

    @HystrixCommand(threadPoolKey = "getThreadPool", fallbackMethod = "buildFallback")
    @Override
    public PathologicalPersonalHistory findPathologicalPersonalHistoryById(Integer id) {
        return PathologicalPersonalHistoryRepository.findById(id).orElse(null);
    }

    @HystrixCommand(threadPoolKey = "updateThreadPool")
    @Override
    public PathologicalPersonalHistory updatePathologicalPersonalHistory(PathologicalPersonalHistory newPathologicalPersonalHistory) {
        Integer id = newPathologicalPersonalHistory.getId_pathological_personal_history();

        PathologicalPersonalHistory oldPathologicalPersonalHistory = PathologicalPersonalHistoryRepository.findById(id).get();

        oldPathologicalPersonalHistory.setChildInfections(newPathologicalPersonalHistory.getChildInfections());
        oldPathologicalPersonalHistory.setVenerealDis(newPathologicalPersonalHistory.getVenerealDis());
        oldPathologicalPersonalHistory.setSurgicalInterv(newPathologicalPersonalHistory.getSurgicalInterv());
        oldPathologicalPersonalHistory.setTraumas(newPathologicalPersonalHistory.getTraumas());
        oldPathologicalPersonalHistory.setLossOfConsciousnes(newPathologicalPersonalHistory.getLossOfConsciousnes());
        oldPathologicalPersonalHistory.setMedicineAllergies(newPathologicalPersonalHistory.getMedicineAllergies());
        oldPathologicalPersonalHistory.setBloodTransfusions(newPathologicalPersonalHistory.getBloodTransfusions());
        return PathologicalPersonalHistoryRepository.save(oldPathologicalPersonalHistory);
    }

    @HystrixCommand
    @Override
    public PathologicalPersonalHistory buildFallback(Integer id) {
        PathologicalPersonalHistory fallback = new PathologicalPersonalHistory();
        fallback.setBloodTransfusions("No info found");
        fallback.setChildInfections("No info found");
        fallback.setLossOfConsciousnes("No info found");
        fallback.setVenerealDis("No info found");
        fallback.setTraumas("No info found");
        fallback.setMedicineAllergies("No info found");
        fallback.setSurgicalInterv("No info found");
        return fallback;
    }

}
