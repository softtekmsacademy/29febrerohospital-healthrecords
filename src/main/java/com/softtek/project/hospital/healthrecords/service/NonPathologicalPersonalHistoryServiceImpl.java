package com.softtek.project.hospital.healthrecords.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.project.hospital.healthrecords.model.NonPathologicalPersonalHistory;
import com.softtek.project.hospital.healthrecords.repository.NonPathologicalPersonalHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty( name = "coreSize", value = "20" ),
                @HystrixProperty( name = "maxQueueSize", value = "10") },
        commandProperties = {
                @HystrixProperty( name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                @HystrixProperty( name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty( name = "circuitBreaker.errorThresholdPercentage", value = "30"),
                @HystrixProperty( name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.timeInMilliseconds", value = "5000"),
                @HystrixProperty( name = "metrics.rollingStats.numBuckets", value = "10") } )

public class NonPathologicalPersonalHistoryServiceImpl implements NonPathologicalPersonalHistoryService {
    @Autowired
    NonPathologicalPersonalHistoryRepository nonPathologicalPersonalHistoryRepository;

    @HystrixCommand(threadPoolKey = "getThreadPool", fallbackMethod = "buildFallback")
    @Override
    public NonPathologicalPersonalHistory findNonPathologicalPersonalHistoryById(Integer id) {
        return nonPathologicalPersonalHistoryRepository.findById(id).orElse(null);
    }

    @HystrixCommand(threadPoolKey = "updateThreadPool")
    @Override
    public NonPathologicalPersonalHistory updateNonPathologicalPersonalHistory(NonPathologicalPersonalHistory newNonPathologicalPersonalHistory) {
        Integer id = newNonPathologicalPersonalHistory.getId_non_pathological_personal_history();

        NonPathologicalPersonalHistory oldNonPathologicalPersonalHistory = nonPathologicalPersonalHistoryRepository.findById(id).get();

        oldNonPathologicalPersonalHistory.setBath(newNonPathologicalPersonalHistory.getBath());
        oldNonPathologicalPersonalHistory.setDefecation(newNonPathologicalPersonalHistory.getDefecation());
        oldNonPathologicalPersonalHistory.setToothBrushing(newNonPathologicalPersonalHistory.getToothBrushing());
        oldNonPathologicalPersonalHistory.setRoomConditions(newNonPathologicalPersonalHistory.getRoomConditions());
        oldNonPathologicalPersonalHistory.setSmoking(newNonPathologicalPersonalHistory.getSmoking());
        oldNonPathologicalPersonalHistory.setAlcoholism(newNonPathologicalPersonalHistory.getAlcoholism());
        oldNonPathologicalPersonalHistory.setToxiconomies(newNonPathologicalPersonalHistory.getToxiconomies());
        oldNonPathologicalPersonalHistory.setFeeding(newNonPathologicalPersonalHistory.getFeeding());
        oldNonPathologicalPersonalHistory.setSports(newNonPathologicalPersonalHistory.getSports());
        oldNonPathologicalPersonalHistory.setScholarship(newNonPathologicalPersonalHistory.getScholarship());
        oldNonPathologicalPersonalHistory.setImmunizations(newNonPathologicalPersonalHistory.getImmunizations());
        oldNonPathologicalPersonalHistory.setAllergies(newNonPathologicalPersonalHistory.getAllergies());
        oldNonPathologicalPersonalHistory.setWorkRest(newNonPathologicalPersonalHistory.getWorkRest());
        oldNonPathologicalPersonalHistory.setHobbies(newNonPathologicalPersonalHistory.getHobbies());
        return nonPathologicalPersonalHistoryRepository.save(oldNonPathologicalPersonalHistory);
    }

    @HystrixCommand
    @Override
    public NonPathologicalPersonalHistory buildFallback(Integer id) {
        NonPathologicalPersonalHistory fallback = new NonPathologicalPersonalHistory();
        fallback.setBath("No info found");
        fallback.setDefecation("No info found");
        fallback.setToothBrushing("No info found");
        fallback.setRoomConditions("No info found");
        fallback.setSmoking("No info found");
        fallback.setAlcoholism("No info found");
        fallback.setToxiconomies("No info found");
        fallback.setFeeding("No info found");
        fallback.setSports("No info found");
        fallback.setScholarship("No info found");
        fallback.setImmunizations("No info found");
        fallback.setAllergies("No info found");
        fallback.setWorkRest("No info found");
        fallback.setHobbies("No info found");
        return fallback;
    }

}
