package com.softtek.project.hospital.healthrecords.model;
import javax.persistence.*;

@Entity
@Table(name = "gynecology_obstetrical_history")
public class GynecologyObstetricalHistory {

    @Id
    @Column(name = "id_gynecology_obstetrical_history")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdGynecologyObstetricalHistory;

    private String menarche;
    private String sexualDevelopment;
    private String menstrualRhythm;
    private String sexLife;
    private String childBirths;
    private Integer abortions;
    private String CaesareanSections;
    private String contraceptiveMeth;
    private String menopause;

    public GynecologyObstetricalHistory(String menarche, String sexualDevelopment, String menstrualRhythm, String sexLife, String childBirths, Integer abortions, String CaesareanSections, String contraceptiveMeth, String menopause) {
        this.menarche = menarche;
        this.sexualDevelopment = sexualDevelopment;
        this.menstrualRhythm = menstrualRhythm;
        this.sexLife = sexLife;
        this.childBirths = childBirths;
        this.abortions = abortions;
        this.CaesareanSections = CaesareanSections;
        this.contraceptiveMeth = contraceptiveMeth;
        this.menopause = menopause;
    }

    public GynecologyObstetricalHistory() {
    }

    public String getMenarche() {
        return menarche;
    }

    public void setMenarche(String menarche) {
        this.menarche = menarche;
    }

    public String getSexualDevelopment() {
        return sexualDevelopment;
    }

    public void setSexualDevelopment(String sexualDevelopment) {
        this.sexualDevelopment = sexualDevelopment;
    }

    public String getMenstrualRhythm() {
        return menstrualRhythm;
    }

    public void setMenstrualRhythm(String menstrualRhythm) {
        this.menstrualRhythm = menstrualRhythm;
    }

    public String getSexLife() {
        return sexLife;
    }

    public void setSexLife(String sexLife) {
        this.sexLife = sexLife;
    }

    public String getChildBirths() {
        return childBirths;
    }

    public void setChildBirths(String childBirths) {
        this.childBirths = childBirths;
    }

    public Integer getAbortions() {
        return abortions;
    }

    public void setAbortions(Integer abortions) {
        this.abortions = abortions;
    }

    public String getCesareanSections() {
        return CaesareanSections;
    }

    public void setCesareanSections(String CaesareanSections) {
        this.CaesareanSections = CaesareanSections;
    }

    public String getContraceptiveMeth() {
        return contraceptiveMeth;
    }

    public void setContraceptiveMeth(String contraceptiveMeth) {
        this.contraceptiveMeth = contraceptiveMeth;
    }

    public String getMenopause() {
        return menopause;
    }

    public void setMenopause(String menopause) {
        this.menopause = menopause;
    }

    public Integer getId() {
        return IdGynecologyObstetricalHistory;
    }

    public void setId(Integer idGynecologyObstetricalHistory) {
        this.IdGynecologyObstetricalHistory = idGynecologyObstetricalHistory;
    }

    @Override
    public String toString() {
        return "GynecologyObstetricalHistory{" +
                "IdGynecologyObstetricalHistory=" + IdGynecologyObstetricalHistory +
                ", menarche='" + menarche + '\'' +
                ", sexualDevelopment='" + sexualDevelopment + '\'' +
                ", menstrualRhythm='" + menstrualRhythm + '\'' +
                ", sexLife='" + sexLife + '\'' +
                ", childBirths='" + childBirths + '\'' +
                ", abortions=" + abortions +
                ", CaesareanSections='" + CaesareanSections + '\'' +
                ", contraceptiveMeth='" + contraceptiveMeth + '\'' +
                ", menopause='" + menopause + '\'' +
                '}';
    }
}