package com.softtek.project.hospital.healthrecords.service;

import com.softtek.project.hospital.healthrecords.model.HealthRecord;

public interface HealthRecordService {
    HealthRecord findHealthRecordById(Integer healthRecordId);
    HealthRecord buildFallback(Integer id);
}
