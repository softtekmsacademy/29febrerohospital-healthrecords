package com.softtek.project.hospital.healthrecords.controller;

import com.softtek.project.hospital.healthrecords.model.NonPathologicalPersonalHistory;
import com.softtek.project.hospital.healthrecords.model.HealthRecord;
import com.softtek.project.hospital.healthrecords.service.NonPathologicalPersonalHistoryService;
import com.softtek.project.hospital.healthrecords.service.HealthRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/healthRecord") // This means URL's start with /healthRecord (after Application path)
public class NonPathologicalPersonalHistoryController {
    @Autowired
    private NonPathologicalPersonalHistoryService nonPathologicalPersonalHistoryService;

    @Autowired
    private HealthRecordService healthRecordService;

    @Autowired
    public NonPathologicalPersonalHistoryController(){
    }

    @GetMapping(path="/{healthRecordId}/nonpathologicalpersonal",
            produces = "application/json")
    public @ResponseBody
    ResponseEntity<String> getNonPathologicalPersonalHistoryByHealthRecordId(
            @PathVariable(value="healthRecordId") Integer healthRecordId) {

        NonPathologicalPersonalHistory nonPathologicalPersonalHistory = null;
        HealthRecord hr = healthRecordService.findHealthRecordById(healthRecordId);

        if(hr != null) {
            nonPathologicalPersonalHistory = hr.getNonpathologicalPersonalHistory();
            if(nonPathologicalPersonalHistory != null) {
                Integer id = nonPathologicalPersonalHistory.getId_non_pathological_personal_history();

                nonPathologicalPersonalHistory = nonPathologicalPersonalHistoryService.findNonPathologicalPersonalHistoryById(id);

                try {
                    return new ResponseEntity(nonPathologicalPersonalHistory, HttpStatus.OK);
                }catch (Exception e){
                    return new ResponseEntity<>("Error getting non pathological personal history", HttpStatus.OK);
                }
            }else{
                return new ResponseEntity<>("health record does not have non pathological personal history", HttpStatus.CONFLICT);
            }
        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }
    }

    @PutMapping(path="/{healthRecordId}/nonpathologicalpersonal",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody
    ResponseEntity<String> putNonPathologicalPersonalHistoryById(
            @PathVariable(value="healthRecordId") Integer healthRecordId,
            @RequestBody NonPathologicalPersonalHistory newNonPathologicalPersonalHistory) {

        Integer idNonPath;
        NonPathologicalPersonalHistory oldNonPathologicalPersonalHistory;

        HealthRecord hr = healthRecordService.findHealthRecordById(healthRecordId);

        if(hr != null) {
            oldNonPathologicalPersonalHistory = hr.getNonpathologicalPersonalHistory();
            if (oldNonPathologicalPersonalHistory != null) {
                idNonPath = oldNonPathologicalPersonalHistory.getId_non_pathological_personal_history();
                newNonPathologicalPersonalHistory.setId_non_pathological_personal_history(idNonPath);

                NonPathologicalPersonalHistory update = nonPathologicalPersonalHistoryService.updateNonPathologicalPersonalHistory(newNonPathologicalPersonalHistory);

                if(update != null){
                    return new ResponseEntity<>("non pathological personal history updated", HttpStatus.OK);
                }else {
                    return new ResponseEntity<>("non pathological personal history not updated",HttpStatus.INTERNAL_SERVER_ERROR);
                }

            }else{
                return new ResponseEntity<>("health record does not have non pathological personal history", HttpStatus.CONFLICT);
            }
        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }


    }


}
