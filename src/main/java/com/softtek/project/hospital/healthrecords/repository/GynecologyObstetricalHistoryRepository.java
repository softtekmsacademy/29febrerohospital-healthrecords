package com.softtek.project.hospital.healthrecords.repository;

import com.softtek.project.hospital.healthrecords.model.GynecologyObstetricalHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GynecologyObstetricalHistoryRepository extends JpaRepository<GynecologyObstetricalHistory, Integer> {

}

