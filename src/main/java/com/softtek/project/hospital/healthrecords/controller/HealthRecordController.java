package com.softtek.project.hospital.healthrecords.controller;

import com.softtek.project.hospital.healthrecords.model.HealthRecord;
import com.softtek.project.hospital.healthrecords.service.HealthRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller // This means that this class is a Controller
@RequestMapping(path="/healthRecord") // This means URL's start with /healthRecord (after Application path)
public class HealthRecordController {
    @Autowired // This means to get the bean called health Record
    private HealthRecordService healthRecordService;

    @Autowired
    public HealthRecordController(){
    }

    @GetMapping(path="/{healthRecordId}")
    public @ResponseBody
    ResponseEntity<String> getHealthRecordById(@PathVariable(value="healthRecordId") Integer healthRecordId) {

        HealthRecord healthRecord = healthRecordService.findHealthRecordById(healthRecordId);

        if(healthRecord != null){
            try{
                return new ResponseEntity(healthRecord, HttpStatus.OK);
            }catch (Exception e){
                return new ResponseEntity<>("Error getting healthrecord",HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }else{
            return new ResponseEntity<>("health record id not found", HttpStatus.CONFLICT);
        }
    }


}