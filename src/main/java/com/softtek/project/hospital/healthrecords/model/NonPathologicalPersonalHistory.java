package com.softtek.project.hospital.healthrecords.model;
import javax.persistence.*;

@Entity
@Table(name = "non_pathological_personal_history")
public class NonPathologicalPersonalHistory {

    @Id
    @Column(name = "id_non_pathological_personal_history")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdNonPathologicalPersonalHistory;

    private String bath;
    private String defecation;
    private String toothBrushing;
    private String roomConditions;
    private String smoking;
    private String alcoholism;
    private String toxiconomies;
    private String feeding;
    private String sports;
    private String scholarship;
    private String immunizations;
    private String allergies;
    private String workRest;
    private String hobbies;

    public NonPathologicalPersonalHistory() {
    }
    public NonPathologicalPersonalHistory(String bath, String defecation, String toothBrushing, String roomConditions, String smoking, String alcoholism, String toxiconomies, String feeding, String sports, String scholarship, String immunizations, String allergies, String workRest, String hobbies) {
        this.bath = bath;
        this.defecation = defecation;
        this.toothBrushing = toothBrushing;
        this.roomConditions = roomConditions;
        this.smoking = smoking;
        this.alcoholism = alcoholism;
        this.toxiconomies = toxiconomies;
        this.feeding = feeding;
        this.sports = sports;
        this.scholarship = scholarship;
        this.immunizations = immunizations;
        this.allergies = allergies;
        this.workRest = workRest;
        this.hobbies = hobbies;
    }

    public String getBath() {
        return bath;
    }

    public void setBath(String bath) {
        this.bath = bath;
    }

    public String getDefecation() {
        return defecation;
    }

    public void setDefecation(String defecation) {
        this.defecation = defecation;
    }

    public String getToothBrushing() {
        return toothBrushing;
    }

    public void setToothBrushing(String toothBrushing) {
        this.toothBrushing = toothBrushing;
    }

    public String getRoomConditions() {
        return roomConditions;
    }

    public void setRoomConditions(String roomConditions) {
        this.roomConditions = roomConditions;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getAlcoholism() {
        return alcoholism;
    }

    public void setAlcoholism(String alcoholism) {
        this.alcoholism = alcoholism;
    }

    public String getToxiconomies() {
        return toxiconomies;
    }

    public void setToxiconomies(String toxiconomies) {
        this.toxiconomies = toxiconomies;
    }

    public String getFeeding() {
        return feeding;
    }

    public void setFeeding(String feeding) {
        this.feeding = feeding;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getScholarship() {
        return scholarship;
    }

    public void setScholarship(String scholarship) {
        this.scholarship = scholarship;
    }

    public String getImmunizations() {
        return immunizations;
    }

    public void setImmunizations(String immunizations) {
        this.immunizations = immunizations;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getWorkRest() {
        return workRest;
    }

    public void setWorkRest(String workRest) {
        this.workRest = workRest;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public Integer getId_non_pathological_personal_history() {
        return IdNonPathologicalPersonalHistory;
    }

    public void setId_non_pathological_personal_history(Integer IdNonPathologicalPersonalHistory) {
        this.IdNonPathologicalPersonalHistory = IdNonPathologicalPersonalHistory;
    }
}
